package com.ppc.marcela.OpenCV;

import static org.opencv.core.Core.LUT;
import static org.opencv.core.CvType.CV_8UC1;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class OpenCVUtils {

    /**
     * Transform a given image to grayscale.
     *
     * @param imageRGBA Image in RGBA format.
     * @return GrayScale of the image in RGBA format.
     */
    @NotNull
    public static Mat grayScaleImage(@NotNull Mat imageRGBA) {
        Mat resultImage = new Mat();
        Imgproc.cvtColor(imageRGBA, resultImage, Imgproc.COLOR_RGBA2GRAY);
        Imgproc.cvtColor(resultImage, resultImage, Imgproc.COLOR_GRAY2BGRA);
        return resultImage;
    }


    /**
     * Transform a given image to have specified n shades of gray.
     *
     * @param imageRGBA Image in RGBA format.
     * @return image after filter in RGBA format.
     */
    @NotNull
    public static Mat LUTGrayScaleImage(@NotNull Mat imageRGBA, int nShades) {
        assert nShades > 1 && nShades < 256: "given: " + nShades;

        Mat resultImage = new Mat();
        Imgproc.cvtColor(imageRGBA, resultImage, Imgproc.COLOR_BGR2GRAY);
        reduceColorsGray(resultImage, nShades);
        Imgproc.cvtColor(resultImage, resultImage, Imgproc.COLOR_GRAY2RGBA);
        return resultImage;
    }

    static void reduceColorsGray(Mat img, int numColors) {
        Mat LUT = createLUT(numColors);
        LUT(img, LUT, img);
    }

    @NotNull
    static Mat createLUT(int numColors) {
        Mat lookupTable = Mat.zeros(new Size(1, 256), CV_8UC1);

        int startIdx = 0;
        for (int x = 0; x < 256; x += 256.0 / numColors) {
            lookupTable.put(x, 0, x);

            for (int y = startIdx; y < x; y++) {
                if (lookupTable.get(y, 0)[0] == 0) {
                    lookupTable.put(y, 0, lookupTable.get(x, 0));
                }
            }
            startIdx = x;
        }
        return lookupTable;
    }

    /**
     * Transform a given image to have color map Summer.
     *
     * @param imageRGBA Image in RGBA format.
     * @return image after filter in RGBA format.
     */
    @NotNull
    public static Mat applyColorMapPink(@NotNull Mat imageRGBA) {
        Mat resultImage = new Mat();
        Imgproc.cvtColor(imageRGBA, resultImage, Imgproc.COLOR_RGBA2RGB);
        Imgproc.applyColorMap(resultImage, resultImage, Imgproc.COLORMAP_PINK);
        Imgproc.cvtColor(resultImage, resultImage, Imgproc.COLOR_RGB2RGBA);
        return resultImage;
    }


    /**
     * Changes the hue of the image
     *
     * @param imageRGBA Image to clip.
     * @param hueToAdd    value between [0, 255) to add to the hue of the image.
     * @return RGBA image changed with the given changeHSV.
     */
    @NotNull
    public static Mat applyHSVChange(@NotNull Mat imageRGBA, double hueToAdd) {
        assert hueToAdd >= 0 && hueToAdd < 256: "Given hueToAdd";

        Scalar changeHSV = new Scalar(hueToAdd, 0.0, 0.0, 0.0);
        Mat resultImage = new Mat();
        Imgproc.cvtColor(imageRGBA, resultImage, Imgproc.COLOR_RGB2HSV_FULL);
        Core.add(resultImage, changeHSV, resultImage);
        Imgproc.cvtColor(resultImage, resultImage, Imgproc.COLOR_HSV2RGB_FULL, 4);
        return resultImage;
    }

    /**
     * Apply laplacian to an image. Create the effect of edges in the picture.
     *
     * @param imageRGBA image to sharpen in RGBA.
     * @param alpha     percentage of effect power, [0, 100]
     */
    public static void applyLaplacianToImage(@NotNull Mat imageRGBA, int alpha) {
        float[][] laplacianKernel = {{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}};
        assert alpha >= 0 && alpha <= 100;
        convolveMatrixWithAlpha(imageRGBA, alpha, laplacianKernel);
    }

    /**
     * Apply gaussian blurring to an image.
     *
     * @param imageRGBA image to sharpen in RGBA.
     * @param alpha     percentage of effect power, [0, 100].
     * @return Sharpened image.
     */
    public static void applyGaussianBlurToImage(@NotNull Mat imageRGBA, int alpha) {
        assert alpha >= 0 && alpha <= 100;
        Mat before = imageRGBA.clone();
        Imgproc.GaussianBlur(before, imageRGBA, new Size(29, 29), 0, 0);
        alphaFeatheringBetween2Images(before, imageRGBA, alpha, imageRGBA);
    }

    /**
     * Normalize the kernel.
     *
     * @param kernel kernel to normalize.
     * @param size   The size of the kernel.
     */
    public static void normalizeKernel(float[][] kernel, int size) {
        float sum = 0;
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                sum += kernel[row][col];
            }
        }
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                kernel[row][col] /= sum;
            }
        }
    }

    /**
     * Convolve a matrix with the given kernel.
     *
     * @param matrix    matrix to convolve.
     * @param alpha     percentage of effect power, [0, 100].
     * @param kernel3x3 kernel to convolve with.
     */
    @NotNull
    public static void convolveMatrixWithAlpha(@NotNull Mat matrix, int alpha, float[][] kernel3x3) {
        Mat sharpenedImage = convolveMatrix(matrix, kernel3x3);
        alphaFeatheringBetween2Images(matrix, sharpenedImage, alpha, matrix);
        sharpenedImage.release();
    }

    @NonNull
    private static void alphaFeatheringBetween2Images(@NonNull Mat matrix, @NonNull Mat sharpenedImage, int alpha, Mat resultImage) {
        float floatAlpha = alpha / 100F;
        Core.multiply(sharpenedImage, new Scalar(floatAlpha, floatAlpha, floatAlpha, floatAlpha), sharpenedImage);
        Mat alphaImageRGBA = new Mat();
        Core.multiply(matrix, new Scalar(1 - floatAlpha, 1 - floatAlpha, 1 - floatAlpha, 1 - floatAlpha), alphaImageRGBA);
        Core.add(alphaImageRGBA, sharpenedImage, resultImage);
        alphaImageRGBA.release();
    }

    /**
     * Convolve a matrix.
     *
     * @param matrix    matrix to convolve.
     * @param kernel3x3 kernel to convolve with.
     * @return convolved matrix.
     */
    @NotNull
    public static Mat convolveMatrix(@NotNull Mat matrix, float[][] kernel3x3) {
        Mat sharpenedImage = new Mat();
        Mat sharpenKernel = new Mat(3, 3, CvType.CV_32F);
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                sharpenKernel.put(row, col, kernel3x3[row][col]);
            }
        }
        Imgproc.filter2D(matrix, sharpenedImage, -1, sharpenKernel);
        sharpenKernel.release();
        return sharpenedImage;
    }

    public static Bitmap convertMatToBitMap(Mat input) {
        Bitmap bitmap = null;
        Mat rgbMat = new Mat();
        Imgproc.cvtColor(input, rgbMat, Imgproc.COLOR_RGBA2RGB);

        try {
            bitmap = Bitmap.createBitmap(rgbMat.cols(), rgbMat.rows(), Bitmap.Config.ARGB_8888);     // TODO-Sahar: There is something wrong here.
            Utils.matToBitmap(input, bitmap);
        } catch (CvException e) {
        }

        return bitmap;
    }
}
