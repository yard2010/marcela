package com.ppc.marcela.Utils

import android.app.Activity
import android.net.Uri
import android.widget.VideoView
import com.ppc.marcela.R

class VideoUtils {

    companion object {
        fun getVideoPath(activity: Activity, rawResource: Int): String =
            "android.resource://" + activity.getPackageName() + "/" + rawResource

        fun setScreenVideo(videoView: VideoView, pathToVideo: String) {
            val uri = Uri.parse(pathToVideo)
            if (uri != null) {
                videoView.setVideoURI(uri)
                videoView.setOnPreparedListener {
                    it.isLooping = true
                    val videoRatio = it.videoWidth / it.videoHeight.toFloat()
                    val screenRatio = videoView.width / videoView.height.toFloat()
                    val scaleX = videoRatio / screenRatio
                    if (scaleX >= 1f) {
                        videoView.scaleX = scaleX
                    } else {
                        videoView.scaleY = 1f / scaleX
                    }
                    videoView.start()
                }
                videoView.start()
            }
        }
    }
}