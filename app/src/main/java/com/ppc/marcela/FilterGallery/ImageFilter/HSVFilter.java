package com.ppc.marcela.FilterGallery.ImageFilter;

import androidx.annotation.NonNull;

import com.ppc.marcela.OpenCV.OpenCVUtils;

import org.opencv.core.Mat;

import java.util.Collections;

public class HSVFilter extends ImageFilter {
    private static final int MAX_VALUE = 255;
    final private FilterParameter filterParameter1;

    /**
     * Hue Saturation and value is HSV format. Apply a filter that adds the scalar HSV to the image.
     *
     * @param hueToAdd value between [0, 255] to add to the hue of the image.
     */
    public HSVFilter(int hueToAdd) {
        assert hueToAdd >= 0 && hueToAdd <= MAX_VALUE;

        this.filterParameter1 = new FilterParameter("Hue To Add", FilterParametersDescriptor.FilterParameterType.Integer, hueToAdd, MAX_VALUE, 0);
    }

    @Override
    public Mat applyFilter(Mat imageRGBA) {
        return OpenCVUtils.applyHSVChange(imageRGBA, filterParameter1.getValue().intValue());
    }

    @NonNull
    @Override
    public FilterParametersDescriptor getParametersDescriptor() {
        return new FilterParametersDescriptor(
                Collections.singletonList(
                        filterParameter1
                )
        );
    }
}
