package com.ppc.marcela.FilterGallery

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.ppc.marcela.FilterGallery.EditFilterFragment.Companion.ARG_FILTER_ID
import com.ppc.marcela.FilterGallery.ImageFilter.ImageFilter
import com.ppc.marcela.R
import com.ppc.marcela.StyleTransfer.StyleFragment

class FilterGalleryChooseFragment : DialogFragment() {

    private lateinit var viewModelFilterGallery: FilterGalleryViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: FilterGalleryImageAdapter
    private var listener: FilterGalleryChooseFragment.OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val model: FilterGalleryViewModel by viewModels()
        val view = inflater.inflate(R.layout.fragment_filter_gallery_main, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        adapter = FilterGalleryImageAdapter()

        viewModelFilterGallery = ViewModelProvider(this).get(FilterGalleryViewModel::class.java)

        recyclerView.adapter = adapter
        adapter.filtersList = model.filters
        adapter.listenerButtonClick = object : AdapterClickListener {
            override fun onItemClick(filterId: String) {
                activity?.supportFragmentManager?.commit {
                    System.out.println(filterId)
                    val f = viewModelFilterGallery.getImageFilter(filterId)
                    if (f != null) {
                        listener?.onFragmentInteraction(f.filter)
                    }
                }
            }
        }
        return view
    }

    fun setRecyclerViewList(list: List<FilterDataModel>) {
        adapter.filtersList = list
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddEditFilterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            FilterGalleryChooseFragment().apply {

            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FilterGalleryChooseFragment.OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(filter: ImageFilter)
    }
}