package com.ppc.marcela.FilterGallery.ImageFilter;

import androidx.annotation.NonNull;

import com.ppc.marcela.OpenCV.OpenCVUtils;

import org.opencv.core.Mat;

import java.util.Collections;

public class GrayScaleFilter extends ImageFilter {

    /**
     * Make the image turn into grayscale.
     */
    public GrayScaleFilter() {}

    @Override
    public Mat applyFilter(Mat imageRGBA) {
        return OpenCVUtils.grayScaleImage(imageRGBA);
    }

    @NonNull
    @Override
    public FilterParametersDescriptor getParametersDescriptor() {
        return new FilterParametersDescriptor(
                Collections.emptyList()
        );
    }
}
