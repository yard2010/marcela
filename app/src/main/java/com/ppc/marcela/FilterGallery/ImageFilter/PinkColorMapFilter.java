package com.ppc.marcela.FilterGallery.ImageFilter;

import androidx.annotation.NonNull;

import com.ppc.marcela.OpenCV.OpenCVUtils;

import org.opencv.core.Mat;

import java.util.Collections;

public class PinkColorMapFilter extends ImageFilter {


    /**
     * Make the image have pink color map.
     */
    public PinkColorMapFilter() {
    }

    @Override
    public Mat applyFilter(Mat imageRGBA) {
        return OpenCVUtils.applyColorMapPink(imageRGBA);
    }

    @NonNull
    @Override
    public FilterParametersDescriptor getParametersDescriptor() {
        return new FilterParametersDescriptor(
                Collections.emptyList()
        );
    }
}
