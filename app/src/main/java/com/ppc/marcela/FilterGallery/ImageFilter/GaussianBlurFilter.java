package com.ppc.marcela.FilterGallery.ImageFilter;

import androidx.annotation.NonNull;

import com.ppc.marcela.OpenCV.OpenCVUtils;

import org.opencv.core.Mat;

import java.util.Collections;

public class GaussianBlurFilter extends ImageFilter {

    private static final int MAX_VALUE = 100;
    final private FilterParameter filterParameter1;

    /**
     * Apply gaussian blur to the image.
     *
     * @param alpha Number between 0 and 100 that is the percentage of applying the effect.
     */
    public GaussianBlurFilter(int alpha) {
        assert alpha >= 0 && alpha <= MAX_VALUE;
        this.filterParameter1 = new FilterParameter("Alpha", FilterParametersDescriptor.FilterParameterType.Integer, alpha, MAX_VALUE, 0);
    }

    @NonNull
    @Override
    public FilterParametersDescriptor getParametersDescriptor() {
        return new FilterParametersDescriptor(
                Collections.singletonList(
                        filterParameter1
                )
        );
    }

    @Override
    public Mat applyFilter(Mat imageRGBA) {
        int alpha = filterParameter1.getValue().intValue();
        OpenCVUtils.applyGaussianBlurToImage(imageRGBA, alpha);
        return imageRGBA;
    }
}
