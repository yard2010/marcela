package com.ppc.marcela.FilterGallery.ImageFilter;


import org.jetbrains.annotations.NotNull;
import org.opencv.core.Mat;

import java.io.Serializable;

public abstract class ImageFilter implements Serializable {

    @NotNull
    abstract public FilterParametersDescriptor getParametersDescriptor();

    /**
     * Applies the filter to the given image.
     *
     * @param imageRGBA RGBA image.
     * @return RGBA result image.
     */
    public abstract Mat applyFilter(Mat imageRGBA);
}
