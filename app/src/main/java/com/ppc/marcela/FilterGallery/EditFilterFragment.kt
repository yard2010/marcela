package com.ppc.marcela.FilterGallery

import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.fragment.app.commit
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ppc.marcela.FilterGallery.FilterGalleryViewModel.Companion.loadBasicFilters
import com.ppc.marcela.FilterGallery.ImageFilter.FilterParameter
import com.ppc.marcela.FilterGallery.ImageFilter.FilterParametersDescriptor
import com.ppc.marcela.R
import com.ppc.marcela.StyleTransfer.ImageUtils
import android.widget.TextView
import androidx.core.view.children
import org.w3c.dom.Text


class EditFilterFragment : Fragment() {

    companion object {
        const val ARG_FILTER_ID = "filter_id"
        const val CUSTOM_FILTER = "Custom"
        fun newInstance() = EditFilterFragment()
    }

    private lateinit var viewModelEditFilter: EditFilterViewModel
    private lateinit var viewModelFilterGallery: FilterGalleryViewModel

    private var filterParameter: FilterParameter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initBackPress()
        val viewFragment = inflater.inflate(R.layout.fragment_edit_filter, container, false)
        viewModelEditFilter = ViewModelProvider(this).get(EditFilterViewModel::class.java)
        viewModelFilterGallery = ViewModelProvider(this).get(FilterGalleryViewModel::class.java)

        val argumentFilterId = arguments?.getString(ARG_FILTER_ID)
        if (argumentFilterId != null) {
            updateViewModelToHoldFilter(argumentFilterId)
        }
        tryInitFilterParameter()

        initSpinner(viewFragment, argumentFilterId)
        initEdtTextName(viewFragment)
        initParameterViewItems(viewFragment)
        initBottomButtons(viewFragment, argumentFilterId)
        return viewFragment
    }

    private fun initBackPress() {
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            openFilterGalleryMainFragment()
        }
        callback.isEnabled = true
    }

    private fun initBottomButtons(viewFragment: View, argumentFilterId: String?) {
        initSaveButton(viewFragment, argumentFilterId)
        initCancelButton(viewFragment)
        initDeleteButton(viewFragment, argumentFilterId)
    }

    private fun initParameterViewItems(viewFragment: View) {
        initParameterName(viewFragment)
        val valueParameterTextNearSeekBar = initValueParameterTextNearSeekBar(viewFragment)
        initSeekBar(viewFragment, valueParameterTextNearSeekBar)
    }

    private fun tryInitFilterParameter() {
        if (viewModelEditFilter.imageFilter.parametersDescriptor.filterParameters.isNotEmpty()) {
            filterParameter = viewModelEditFilter.imageFilter.parametersDescriptor.filterParameters[0]
        } else {
            filterParameter = null
        }
    }

    private fun initSpinner(viewFragment: View, argumentFilterId: String?) {
        val spinner = viewFragment.findViewById<Spinner>(R.id.spinner)
        val basicFilters = loadBasicFilters()
        if (argumentFilterId != null) {
            val filterDataModel = viewModelFilterGallery.getImageFilter(argumentFilterId)
            if (filterDataModel != null) {
                basicFilters.add(0, FilterDataModel(CUSTOM_FILTER, filterDataModel.filter, filterDataModel.id))
            }
        }
        val adapter = ArrayAdapter(viewFragment.context, R.layout.spinner_row, basicFilters)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, viewParam: View?, pos: Int, id: Long) {
                if (viewParam != null) {
                    (viewParam as TextView).setTextColor(Color.WHITE)   // TODO-Sahar: spinner after rotation stops using spinner_row, so text is black and not white.
                }
                val selectedFilterDataModel = basicFilters[pos]
                if (selectedFilterDataModel.name != CUSTOM_FILTER) {
                    viewModelEditFilter.name = selectedFilterDataModel.name
                }
                viewModelEditFilter.imageFilter = selectedFilterDataModel.filter
                tryInitFilterParameter()
                initParameterViewItems(viewFragment)
                updateImage(viewFragment)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
    }

    private fun updateImage(view: View) {
        val imageBitmap = ImageUtils.applyFilterOnMonaImage(view.context, viewModelEditFilter.imageFilter)
        val imageView = view.findViewById<ImageView>(R.id.exampleImage)
        Glide.with(imageView)
            .load(imageBitmap)
            .into(imageView)
    }

    private fun updateViewModelToHoldFilter(argumentFilterId: String) {
        val filterDataModel = viewModelFilterGallery.getImageFilter(argumentFilterId)
        // Get name of filter.
        if (filterDataModel?.name != null) {
            viewModelEditFilter.name = filterDataModel.name
        }
        // Get the filter.
        if (filterDataModel?.filter != null) {
            viewModelEditFilter.imageFilter = filterDataModel.filter
        }
    }

    private fun initParameterName(view: View) {
        val parameterName = view.findViewById<TextView>(R.id.parameterText)
        if (filterParameter != null) {
            parameterName.visibility = View.VISIBLE
            parameterName.text = filterParameter?.name + ":"
        } else {
            parameterName.visibility = View.GONE
        }
    }

    private fun initValueParameterTextNearSeekBar(view: View): TextView {
        val valueParameterText = view.findViewById<TextView>(R.id.currentParameterText)
        if (filterParameter != null) {
            valueParameterText.visibility = View.VISIBLE
            valueParameterText.text = filterParameter?.value.toString()
        } else {
            valueParameterText.visibility = View.GONE
        }
        return valueParameterText
    }

    private fun initSeekBar(view: View, valueParameterText: TextView) {
        val parameterSeekBar = view.findViewById<SeekBar>(R.id.parameterSeekBar)
        if (filterParameter != null) {
            parameterSeekBar.visibility = View.VISIBLE
            initSeekbarProgress(parameterSeekBar)
            initSeekBarChangeListener(parameterSeekBar, valueParameterText)
        } else {
            parameterSeekBar.visibility = View.GONE
        }
    }

    private fun initSeekBarChangeListener(
        parameterSeekBar: SeekBar,
        valueParameterText: TextView
    ) {
        parameterSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, p1: Int, p2: Boolean) {
                valueParameterText.text = p1.toString()
                viewModelEditFilter.imageFilter.parametersDescriptor.filterParameters[0].value = p1

                // update image:
                updateImage(seekBar.rootView)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })
    }

    private fun initSeekbarProgress(parameterSeekBar: SeekBar) {
        if (filterParameter?.type == FilterParametersDescriptor.FilterParameterType.Integer) {
            parameterSeekBar.progress = filterParameter?.value?.toInt() ?: 0
            parameterSeekBar.max = filterParameter?.maxValue?.toInt() ?: 100
            parameterSeekBar.min = filterParameter?.minValue?.toInt() ?: 0
        }
    }

    private fun initDeleteButton(view: View, argumentFilterId: String?) {
        val deleteButton = view.findViewById<Button>(R.id.deleteButton)
        if (argumentFilterId != null) {
            deleteButton.visibility = View.VISIBLE
            deleteButton.setOnClickListener {
                viewModelFilterGallery.removeFilter(argumentFilterId)
                openFilterGalleryMainFragment()
            }
        } else {
            deleteButton.visibility = View.INVISIBLE
        }
    }

    private fun initCancelButton(view: View) {
        val cancelButton = view.findViewById<Button>(R.id.cancelButton)
        cancelButton.setOnClickListener {
            openFilterGalleryMainFragment()
        }
    }

    private fun initSaveButton(view: View, argumentFilterId: String?) {
        val saveButton = view.findViewById<Button>(R.id.saveButton)
        saveButton.setOnClickListener {
            val name = viewModelEditFilter.name
            val filter = viewModelEditFilter.imageFilter
            if (argumentFilterId == null) {
                viewModelFilterGallery.addFilter(FilterDataModel(name, filter))
            } else {
                viewModelFilterGallery.replaceFilter(argumentFilterId, FilterDataModel(name, filter, argumentFilterId))
            }
            openFilterGalleryMainFragment()
        }
    }

    private fun initEdtTextName(view: View) {
        val editTextName = view.findViewById<EditText>(R.id.filterNameEdit)
        editTextName.setText(viewModelEditFilter.name)
        editTextName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                viewModelEditFilter.name = p0.toString()
            }
        })
    }

    private fun openFilterGalleryMainFragment() {
        activity?.supportFragmentManager?.commit {
            setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
            replace(R.id.fragment_container_view, FilterGalleryMainFragment.newInstance())
        }
    }

}