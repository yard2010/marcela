package com.ppc.marcela.FilterGallery

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ppc.marcela.R
import com.ppc.marcela.StyleTransfer.ImageUtils.Companion.applyFilterOnMonaImage

interface AdapterClickListener {
    fun onItemClick(filterId: String)
}

class FilterGalleryImageAdapter() :
    RecyclerView.Adapter<FilterGalleryImageAdapter.FilterViewHolder>() {


    var filtersList = emptyList<FilterDataModel>()
        set(newFilterList) {
            field = newFilterList
            notifyDataSetChanged()
        }

    var listenerButtonClick: AdapterClickListener? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.filter_image_layout, parent, false)
        return FilterViewHolder(view)
    }

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        val filterDataModel = filtersList[position]

        val context = holder.itemView.context
        val exampleFilterResultBitmap = applyFilterOnMonaImage(context, filterDataModel.filter)

        Glide.with(holder.imageView)
            .load(exampleFilterResultBitmap)
            .into(holder.imageView)

        holder.imageView.setOnClickListener {
            Log.d("imageview", "Clicked $position")
            listenerButtonClick?.onItemClick(filterDataModel.id)
        }
        // TODO: Apply the filter and reset the preview image   // TODO-Sahar: What do you mean?
    }

    override fun getItemCount() = filtersList.size

    class FilterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView = itemView.findViewById(R.id.imageView)
    }
}