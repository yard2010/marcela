package com.ppc.marcela.FilterGallery.ImageFilter

data class FilterParameter(
    var name: String,
    val type: FilterParametersDescriptor.FilterParameterType,
    var value: Number,
    val maxValue: Number,
    val minValue: Number = 0
)