package com.ppc.marcela.FilterGallery.ImageFilter;

import androidx.annotation.NonNull;

import com.ppc.marcela.OpenCV.OpenCVUtils;

import org.opencv.core.Mat;

import java.util.Collections;

public class LUTGrayScaleFilter extends ImageFilter {

    private static final int MAX_VALUE = 50;
    private static final int MIN_VALUE = 2;
    final private FilterParameter filterParameter1;

    /**
     * Make the image turn into grayscale image using specified number of gray colors.
     *
     * @param nShades: number of shades of gray, number between [2, 50] (can be up to 255, but it
     *                 will not cause an effect).
     */
    public LUTGrayScaleFilter(int nShades) {
        this.filterParameter1 = new FilterParameter("Num Shades", FilterParametersDescriptor.FilterParameterType.Integer, nShades, MAX_VALUE, MIN_VALUE);
    }

    @Override
    public Mat applyFilter(Mat imageRGBA) {
        return OpenCVUtils.LUTGrayScaleImage(imageRGBA, filterParameter1.getValue().intValue());
    }

    @NonNull
    @Override
    public FilterParametersDescriptor getParametersDescriptor() {
        return new FilterParametersDescriptor(
                Collections.singletonList(
                        filterParameter1
                )
        );
    }
}
