package com.ppc.marcela.FilterGallery

import com.ppc.marcela.FilterGallery.ImageFilter.ImageFilter
import java.util.*

data class FilterDataModel(
    var name: String,
    var filter: ImageFilter,
    var id: String = UUID.randomUUID().toString()
) {
    override fun toString(): String {
        return name
    }
}
