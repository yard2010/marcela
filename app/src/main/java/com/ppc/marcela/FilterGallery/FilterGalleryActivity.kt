package com.ppc.marcela.FilterGallery

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.VideoView
import androidx.fragment.app.commit
import com.ppc.marcela.R
import com.ppc.marcela.Utils.VideoUtils.Companion.getVideoPath
import com.ppc.marcela.Utils.VideoUtils.Companion.setScreenVideo
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.android.BaseLoaderCallback


const val GALLERY_LOG_TAG = "FilterGalleryTag"

class FilterGalleryActivity : AppCompatActivity() {

    private lateinit var filterGalleryMainFragment: FilterGalleryMainFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_gallery)
        setSupportActionBar(findViewById(R.id.topBar))
        supportActionBar?.title = "Filters Gallery"

        if (savedInstanceState == null) {
            filterGalleryMainFragment = FilterGalleryMainFragment.newInstance()
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                replace(R.id.fragment_container_view, filterGalleryMainFragment)
            }
        }

        initBackgroundVideo()
        initOpenCV()
    }

    private fun initOpenCV() {
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mOpenCVCallBack);
        } else {
            mOpenCVCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    private fun initBackgroundVideo() {
        val videoView = findViewById<VideoView>(R.id.backgroundVideo)
        setScreenVideo(videoView, getVideoPath(this, R.raw.grid))
    }
    private val mOpenCVCallBack: BaseLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                SUCCESS -> {
                    Log.i(GALLERY_LOG_TAG, "OpenCV Loaded Sucessfully")
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.filters_gallery, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_add -> {
            // TODO: add filter
            Log.d("Action", "Clicked")
            val editFilterFragment = EditFilterFragment.newInstance()
            supportFragmentManager.commit{
                setCustomAnimations(R.anim.slide_out_down, R.anim.slide_in_down)
                replace(R.id.fragment_container_view, editFilterFragment)
            }
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}