package com.ppc.marcela.FilterGallery.ImageFilter;


import android.util.Pair;

import androidx.annotation.NonNull;

import com.ppc.marcela.OpenCV.OpenCVUtils;

import org.opencv.core.Mat;

import java.util.Collections;

import kotlin.Triple;

public class SharpenFilter extends ImageFilter {

    private static final int MAX_VALUE = 100;
    final private FilterParameter filterParameter1;

    /**
     * Apply sharpen to the image.
     *
     * @param alpha Number between 0 and 100 that is the percentage of applying the effect.
     */
    public SharpenFilter(int alpha) {
        assert alpha >= 0 && alpha <= 100;
        this.filterParameter1 = new FilterParameter("Alpha", FilterParametersDescriptor.FilterParameterType.Integer, alpha, MAX_VALUE, 0);
    }

    @Override
    public Mat applyFilter(Mat imageRGBA) {
        OpenCVUtils.applyLaplacianToImage(imageRGBA, filterParameter1.getValue().intValue());
        return imageRGBA;
    }

    @NonNull
    @Override
    public FilterParametersDescriptor getParametersDescriptor() {
        return new FilterParametersDescriptor(
                Collections.singletonList(
                        filterParameter1
                )
        );
    }
}
