package com.ppc.marcela.FilterGallery.ImageFilter

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ppc.marcela.Utils.RuntimeTypeAdapterFactory

class ImageFilterRuntimeTypeAdapterFactory {
    companion object {
        private val typeFactory: RuntimeTypeAdapterFactory<ImageFilter> = RuntimeTypeAdapterFactory
            .of(ImageFilter::class.java, "type")
            .registerSubtype(GaussianBlurFilter::class.java, "GaussianBlurFilter")
            .registerSubtype(GrayScaleFilter::class.java, "GrayScaleFilter")
            .registerSubtype(HSVFilter::class.java, "HSVFilter")
            .registerSubtype(LUTGrayScaleFilter::class.java, "LUTGrayScaleFilter")
            .registerSubtype(PinkColorMapFilter::class.java, "PinkColorMapFilter")
            .registerSubtype(SharpenFilter::class.java, "SharpenFilter")

        fun getFiltersGson(): Gson = GsonBuilder()
            .registerTypeAdapterFactory(typeFactory)
            .create()
    }
}