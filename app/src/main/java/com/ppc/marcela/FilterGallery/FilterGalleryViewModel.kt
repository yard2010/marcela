package com.ppc.marcela.FilterGallery

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.ppc.marcela.FilterGallery.ImageFilter.*
import com.ppc.marcela.FilterGallery.ImageFilter.ImageFilterRuntimeTypeAdapterFactory.Companion.getFiltersGson
import com.ppc.marcela.SP_MAIN
import java.util.*
import kotlin.collections.ArrayList


const val TAG_FILTER = "TAG - Filter in filter gallery - id:"

class FilterGalleryViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        fun loadBasicFilters(): ArrayList<FilterDataModel> {
            return ArrayList(
                Arrays.asList(
                    FilterDataModel("Gray Filter", GrayScaleFilter()),
                    FilterDataModel("5 Shades of Gray", LUTGrayScaleFilter(5)),
                    FilterDataModel("Sharpen", SharpenFilter(40)),
                    FilterDataModel("Gaussian Blur", GaussianBlurFilter(100)),
                    FilterDataModel("Pink", PinkColorMapFilter()),
                    FilterDataModel("Hue", HSVFilter(50)),
                )
            )
        }

        fun loadInitialFilters(): ArrayList<FilterDataModel> {
            return ArrayList(
                Arrays.asList(
                    FilterDataModel("Gray Filter", GrayScaleFilter()),
                    FilterDataModel("5 Shades of Gray", LUTGrayScaleFilter(5)),
                    FilterDataModel("3 Shades of Gray", LUTGrayScaleFilter(3)),
                    FilterDataModel("Sharpen", SharpenFilter(40)),
                    FilterDataModel("Gaussian Blur", GaussianBlurFilter(100)),
                    FilterDataModel("Pink", PinkColorMapFilter()),
                    FilterDataModel("Hue 1", HSVFilter(50)),
                    FilterDataModel("Hue 2", HSVFilter(100)),
                    FilterDataModel("Hue 3", HSVFilter(150)),
                    FilterDataModel("Hue 4", HSVFilter(130)),
                )
            )
        }
    }

    var filters = initFilters()


    fun initFilters(): ArrayList<FilterDataModel> {

        val sp = getApplication<Application>().getSharedPreferences(SP_MAIN, Application.MODE_PRIVATE)
        val filtersAsString = sp.getString(TAG_FILTER, "")
        if (filtersAsString != "") {
            val gson = getFiltersGson()
            return gson.fromJson(filtersAsString, Array<FilterDataModel>::class.java)
                .toList() as ArrayList<FilterDataModel>
        } else {
            val basicFilters = loadInitialFilters()
            saveFiltersInSharedPreferences(basicFilters)
            return basicFilters
        }
    }

    fun addFilter(filter: FilterDataModel) {
        filters.add(filter)
        saveFiltersInSharedPreferences(filters)
    }

    fun replaceFilter(oldFilterId: String, newFilter: FilterDataModel) {
        val indexOfFilter = filters.indexOfFirst { filter -> filter.id == oldFilterId }
        if (indexOfFilter != -1) {
            filters[indexOfFilter] = newFilter
        }
        saveFiltersInSharedPreferences(filters)
    }

    fun removeFilter(filterId: String) {
        filters.removeIf { filter -> filter.id == filterId }
        saveFiltersInSharedPreferences(filters)
    }

    fun removeFilter(filter: FilterDataModel) {
        filters.remove(filter)
        saveFiltersInSharedPreferences(filters)
    }
    private fun saveFiltersInSharedPreferences(filters: ArrayList<FilterDataModel>) {
        val sp = getApplication<Application>().getSharedPreferences(SP_MAIN, Application.MODE_PRIVATE)
        sp.edit().putString(TAG_FILTER, getFiltersGson().toJson(filters)).apply()
    }



    fun getImageFilter(filterId: String): FilterDataModel? {
        return filters.find { it.id == filterId }
    }
}
