package com.ppc.marcela.FilterGallery

import androidx.lifecycle.ViewModel
import com.ppc.marcela.FilterGallery.ImageFilter.GrayScaleFilter
import com.ppc.marcela.FilterGallery.ImageFilter.ImageFilter

class EditFilterViewModel : ViewModel() {
    var name = ""
    var imageFilter: ImageFilter = GrayScaleFilter()
}