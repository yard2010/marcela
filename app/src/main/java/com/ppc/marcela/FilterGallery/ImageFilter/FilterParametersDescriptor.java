package com.ppc.marcela.FilterGallery.ImageFilter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class FilterParametersDescriptor {

    @NotNull
    public List<FilterParameter> getFilterParameters() {
        return filterParameters;
    }

    public enum FilterParameterType {
        Integer, Float, Double
    }

    @NotNull
    private final List<FilterParameter> filterParameters;


    /**
     * @param filterParameters  list of names and type that are the parameters of the filter.
     */
    public FilterParametersDescriptor(@NotNull List<FilterParameter> filterParameters) {
        this.filterParameters = filterParameters;
    }

}
