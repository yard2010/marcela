package com.ppc.marcela

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.*
import android.widget.Button
import android.widget.ImageView
import android.widget.VideoView
import androidx.core.content.ContextCompat
import com.ppc.marcela.FilterGallery.FilterGalleryActivity
import com.ppc.marcela.StyleTransfer.StyleTransferActivity
import com.ppc.marcela.Utils.VideoUtils
import com.ppc.marcela.Utils.VideoUtils.Companion.setScreenVideo
import java.util.ArrayList

class MainPageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_page)

        initBackgroundVideo()

        initButtonStyleTransfer()
        initButtonFilterGallery()
        initImagesShow()
    }

    private fun initBackgroundVideo() {
        val videoView = findViewById<VideoView>(R.id.backgroundVideo)
        setScreenVideo(videoView, VideoUtils.getVideoPath(this, R.raw.grid))
    }

    private fun initImagesShow() {
        val imagesShow = findViewById<ImageView>(R.id.images_show)
        val imagesToShow = ArrayList<Int>()
        imagesToShow.add(R.drawable.image_1)
        imagesToShow.add(R.drawable.image_2)
        imagesToShow.add(R.drawable.image_3)
        imagesToShow.add(R.drawable.image_4)
        imagesToShow.add(R.drawable.image_5)
        imagesToShow.add(R.drawable.image_6)
        imagesToShow.add(R.drawable.image_7)
        imagesToShow.shuffle()
        animateImagesShow(imagesShow, imagesToShow, 0, true)
    }

    private fun initButtonStyleTransfer() {
        val styleButton = findViewById<Button>(R.id.button_style)
        styleButton.setOnClickListener {
            val intent = Intent(this, StyleTransferActivity::class.java)
            ContextCompat.startActivity(this, intent, null)
        }
    }

    private fun initButtonFilterGallery() {
        val filterGalleryButton = findViewById<Button>(R.id.button_filter_gallery)
        filterGalleryButton.setOnClickListener {
            val intent = Intent(this, FilterGalleryActivity::class.java)
            ContextCompat.startActivity(this, intent, null)
        }
    }

    fun animateImagesShow(
        imageView: ImageView,
        imagesToShow: ArrayList<Int>,
        imageIndexToStart: Int,
        shouldRunForever: Boolean
    ) {
        val fadeInDuration = 500L
        val timeBetween = 3000L
        val fadeOutDuration = 1000L

        //Visible or invisible by default - this will apply when the animation ends
        imageView.visibility = View.INVISIBLE
        imageView.setImageResource(imagesToShow[imageIndexToStart])

        val fadeIn = AlphaAnimation(0F, 1F)
        fadeIn.interpolator = DecelerateInterpolator()
        fadeIn.duration = fadeInDuration

        val fadeOut = AlphaAnimation(1F, 0F)
        fadeOut.interpolator = AccelerateInterpolator()
        fadeOut.startOffset = fadeInDuration + timeBetween
        fadeOut.duration = fadeOutDuration

        val animation = AnimationSet(false)
        animation.addAnimation(fadeIn)
        animation.addAnimation(fadeOut)
        animation.repeatCount = 1
        imageView.animation = animation

        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationEnd(animation: Animation) {
                if (imagesToShow.size - 1 > imageIndexToStart) {
                    animateImagesShow(
                        imageView,
                        imagesToShow,
                        imageIndexToStart + 1,
                        shouldRunForever
                    )
                } else {
                    if (shouldRunForever == true) {
                        animateImagesShow(
                            imageView,
                            imagesToShow,
                            0,
                            shouldRunForever
                        )
                    }
                }
            }

            override fun onAnimationRepeat(animation: Animation) {}

            override fun onAnimationStart(animation: Animation) {}
        })
    }
}