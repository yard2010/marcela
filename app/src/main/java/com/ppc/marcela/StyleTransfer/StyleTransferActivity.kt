package com.ppc.marcela.StyleTransfer

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.View.*
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePickerConfig
import com.esafirm.imagepicker.features.ImagePickerMode
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.features.registerImagePicker
import com.google.android.material.appbar.MaterialToolbar
import com.ppc.marcela.FilterGallery.FilterGalleryChooseFragment
import com.ppc.marcela.FilterGallery.GALLERY_LOG_TAG
import com.ppc.marcela.FilterGallery.ImageFilter.ImageFilter
import com.ppc.marcela.R
import kotlinx.coroutines.asCoroutineDispatcher
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.Mat
import java.io.ByteArrayOutputStream
import java.util.concurrent.Executors
import com.ppc.marcela.OpenCV.OpenCVUtils
import com.ppc.marcela.Utils.VideoUtils.Companion.getVideoPath
import com.ppc.marcela.Utils.VideoUtils.Companion.setScreenVideo
import kotlin.random.Random

class StyleTransferActivity :
    AppCompatActivity(),
    StyleFragment.OnListFragmentInteractionListener,
    FilterGalleryChooseFragment.OnFragmentInteractionListener {

    private lateinit var imageView: ImageView
    private lateinit var pickStyleButton: Button
    private lateinit var pickCustomStyleButton: Button
    private lateinit var tryAgainButton: Button
    private lateinit var shareButton: Button
    private lateinit var backButton: Button
    private lateinit var applyFilter: Button

    private var isShowingStyleLoadingVideo: MutableLiveData<Boolean> = MutableLiveData(false)

    private val filterGalleryChooseFragment: FilterGalleryChooseFragment = FilterGalleryChooseFragment()


    private lateinit var viewModel: MLExecutionViewModel
    private lateinit var styleTransferModelExecutor: StyleTransferModelExecutor
    private val inferenceThread = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    private var selectedImageBitmap: Bitmap? = null
    private var selectedStyle: String? = null
    private var selectedCustomStyle: Bitmap? = null
    private val stylesFragment: StyleFragment = StyleFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_style_transfer)
        setSupportActionBar(findViewById(R.id.topBar))

        val backgroundVideo = findViewById<VideoView>(R.id.backgroundVideo)
        setScreenVideo(backgroundVideo, getVideoPath(this, R.raw.grid))

        imageView = findViewById(R.id.imageView)
        pickStyleButton = findViewById(R.id.pickStyleButton)
        pickCustomStyleButton = findViewById(R.id.pickCustomStyleButton)
        tryAgainButton = findViewById(R.id.tryAgainButton)
        shareButton = findViewById(R.id.shareButton)
        backButton = findViewById(R.id.backButton)
        applyFilter = findViewById(R.id.applyFilter)

        viewModel = ViewModelProvider.AndroidViewModelFactory(application).create(MLExecutionViewModel::class.java)

        viewModel.styledBitmap.observe(
            this,
            { resultImage ->
                if (resultImage != null) {
                    if (isShowingStyleLoadingVideo.value == false) {
                        gotResult(resultImage)
                    } else {
                        isShowingStyleLoadingVideo.observe(this, {
                            if (isShowingStyleLoadingVideo.value == false) {
                                gotResult(resultImage)
                            }
                        })
                    }
                } else {
                    finish()
                }
            }
        )

        styleTransferModelExecutor = StyleTransferModelExecutor(this@StyleTransferActivity, true)
        Log.d("MAIN", "Executor created")

        pickStyleButton.setOnClickListener {
            stylesFragment.show(supportFragmentManager, "styles-fragment")
        }

        val customLauncher = registerImagePicker {
            if (it.size > 0) {
                playVideo()
                selectedCustomStyle = ThumbnailUtils.extractThumbnail(imageURIToBitmap(it[0].uri), 512, 512)
                pickStyleButton.visibility = GONE
                pickCustomStyleButton.visibility = GONE
                viewModel.onApplyCustomStyle(
                    baseContext, selectedImageBitmap!!, selectedCustomStyle!!, styleTransferModelExecutor,
                    inferenceThread
                )
            } else {
                finish()
            }
        }
        pickCustomStyleButton.setOnClickListener {
            val config = ImagePickerConfig {
                mode = ImagePickerMode.SINGLE // default is multi image mode
            }

            customLauncher.launch(config)
        }

        backButton.setOnClickListener {
            finish()
        }

        tryAgainButton.setOnClickListener {
            finish()
            startActivity(intent)
        }

        val config = ImagePickerConfig {
            mode = ImagePickerMode.SINGLE // default is multi image mode
            returnMode = ReturnMode.ALL
        }
        val launcher = registerImagePicker {
            if (it.size > 0) {
                selectedImageBitmap = centerCropImage(imageURIToBitmap(it[0].uri))
                Glide.with(imageView)
                    .load(selectedImageBitmap)
                    .fitCenter()
                    .into(imageView)
            } else {
                finish()
            }
        }
        launcher.launch(config)
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
    }

    private fun gotResult(resultImage: ModelExecutionResult) {
        Glide.with(imageView)
            .load(resultImage.styledImage)
            .into(imageView)
        tryAgainButton.visibility = VISIBLE
        shareButton.visibility = VISIBLE
        backButton.visibility = VISIBLE
        applyFilter.visibility = VISIBLE

        shareButton.setOnClickListener {

            val outputStream = ByteArrayOutputStream()
            resultImage.styledImage.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            val path =
                MediaStore.Images.Media.insertImage(contentResolver, resultImage.styledImage, "Styled Image", null)
            Toast.makeText(applicationContext, "Saved to Pictures", Toast.LENGTH_SHORT).show()
            outputStream.close()

            val sendIntent = Intent().apply {
                action = Intent.ACTION_SEND
                type = "image/png"
                putExtra(Intent.EXTRA_STREAM, Uri.parse(path))
            }
            val shareIntent = Intent.createChooser(sendIntent, "Share Result")
            startActivity(shareIntent)
        }

        applyFilter.setOnClickListener {
            tryAgainButton.visibility = GONE
            shareButton.visibility = GONE
            backButton.visibility = GONE
            applyFilter.visibility = GONE
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                replace(R.id.fragment_container_view2, filterGalleryChooseFragment)
            }
            initOpenCV()
        }
    }

    fun imageURIToBitmap(uri: Uri): Bitmap {
        return if (Build.VERSION.SDK_INT < 28) {
            MediaStore.Images.Media.getBitmap(contentResolver, uri)
        } else {
            ImageDecoder.decodeBitmap(ImageDecoder.createSource(contentResolver, uri))
                .copy(Bitmap.Config.RGBA_F16, true)
        }
    }

    fun centerCropImage(bitmap: Bitmap): Bitmap {
        val targetDimension = bitmap.width.coerceAtMost(bitmap.height)
        return ThumbnailUtils.extractThumbnail(bitmap, targetDimension, targetDimension)
    }

    override fun onListFragmentInteraction(item: String) {
        playVideo()
        selectedStyle = item
        stylesFragment.dismiss()
        pickStyleButton.visibility = GONE
        pickCustomStyleButton.visibility = GONE

        viewModel.onApplyStyle(
            baseContext, selectedImageBitmap!!, selectedStyle.toString(), styleTransferModelExecutor,
            inferenceThread
        )
    }

    private fun playVideo() {
        isShowingStyleLoadingVideo.postValue(true)
        val topBar = findViewById<MaterialToolbar>(R.id.topBar)
        val handler = Handler(Looper.getMainLooper())
        val styleLoadingVideo = findViewById<VideoView>(R.id.style_loading_video)
        styleLoadingVideo.visibility = VISIBLE
        styleLoadingVideo.bringToFront()

        val availableVideos = ArrayList<Int>()
        availableVideos.add(R.raw.energy)
        availableVideos.add(R.raw.energy)
        availableVideos.add(R.raw.paint)
        availableVideos.add(R.raw.painter)
        availableVideos.add(R.raw.reading_subway)
        availableVideos.add(R.raw.hawaii)
        availableVideos.add(R.raw.seoul)
        val rawResource = availableVideos[Random.nextInt(0, availableVideos.size)]

        handler.postAtTime({
            setSupportActionBar(null)
            hideSystemUI()
            actionBar?.hide()
            topBar.visibility = INVISIBLE
            setScreenVideo(styleLoadingVideo, getVideoPath(this, rawResource))
            handler.postDelayed({ styleLoadingVideo.visibility = GONE }, 4000)
        }, 4000)

        handler.postDelayed({
            showSystemUI()
            topBar.visibility = VISIBLE
            isShowingStyleLoadingVideo.postValue(false)
        }, 4000)
    }

    override fun onFragmentInteraction(filter: ImageFilter) {
        val alizaMat = Mat()

        val curr = viewModel.styledBitmap.value?.styledImage
        if (curr != null) {
            Utils.bitmapToMat(curr.copy(Bitmap.Config.ARGB_8888, true), alizaMat)
        }
        System.out.println(alizaMat.toString())
        Glide.with(imageView)
            .load(OpenCVUtils.convertMatToBitMap(filter.applyFilter(alizaMat)))
            .into(imageView)
        filterGalleryChooseFragment.dismiss()
        viewModel.updateImage(OpenCVUtils.convertMatToBitMap(filter.applyFilter(alizaMat)))
        tryAgainButton.visibility = VISIBLE
        shareButton.visibility = VISIBLE
        backButton.visibility = VISIBLE
        applyFilter.visibility = VISIBLE
    }

    private val mOpenCVCallBack: BaseLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                SUCCESS -> {
                    Log.i(GALLERY_LOG_TAG, "OpenCV Loaded Sucessfully")
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    private fun initOpenCV() {
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mOpenCVCallBack);
        } else {
            mOpenCVCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }
}