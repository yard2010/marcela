package com.ppc.marcela.StyleTransfer

import android.content.Context
import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExecutorCoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class MLExecutionViewModel : ViewModel() {

    private val _styledBitmap = MutableLiveData<ModelExecutionResult>()

    val styledBitmap: LiveData<ModelExecutionResult>
        get() = _styledBitmap

    private val viewModelJob = Job()
    private val viewModelScope = CoroutineScope(viewModelJob)

    fun onApplyStyle(
        context: Context,
        contentBitmap: Bitmap,
        styleFilePath: String,
        styleTransferModelExecutor: StyleTransferModelExecutor,
        inferenceThread: ExecutorCoroutineDispatcher
    ) {
        viewModelScope.launch(inferenceThread) {
            val loadBitmapFromResources = ImageUtils.loadBitmapFromResources(context, "thumbnails/$styleFilePath")
            val result =
                styleTransferModelExecutor.execute(contentBitmap, loadBitmapFromResources, context)
            _styledBitmap.postValue(result)
        }
    }

    fun onApplyCustomStyle(
        context: Context,
        contentBitmap: Bitmap,
        styleImage: Bitmap,
        styleTransferModelExecutor: StyleTransferModelExecutor,
        inferenceThread: ExecutorCoroutineDispatcher
    ) {
        viewModelScope.launch(inferenceThread) {
            val result =
                styleTransferModelExecutor.execute(contentBitmap, styleImage, context)
            _styledBitmap.postValue(result)
        }
    }

    fun updateImage(
        contentBitmap: Bitmap
    ) {
        val result = ModelExecutionResult(contentBitmap)
        _styledBitmap.postValue(result)

    }
}
